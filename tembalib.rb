require 'pry' # for debugging use a line as `binding.pry` src https://stackoverflow.com/questions/1144560/how-do-i-drop-to-the-irb-prompt-from-a-running-script
require 'erb' # config templates
require 'psych' # DB in a file
require 'ipaddress' # ip validation
require 'archive/zip' # zip stuff

# global variables https://stackoverflow.com/questions/12112765/how-to-reference-global-variables-and-class-variables

# src https://stackoverflow.com/a/27127342
def gen_timestamp()
  return Time.new.strftime ("%Y-%m-%d-%H-%M-%L")
end

def get_current_temba_commit()
  # get latest commit -> src https://stackoverflow.com/questions/949314/how-to-retrieve-the-hash-for-the-current-commit-in-git
  current_commit = `git log --pretty=format:'%h' -n 1` || ''
  # get branch -> src https://stackoverflow.com/a/12142066
  # get rid of new line -> src https://stackoverflow.com/questions/7533318/get-rid-of-newline-from-shell-commands-in-ruby
  #current_branch = `git rev-parse --abbrev-ref HEAD`.chop || ''

  return current_commit
end

# mostly done by src https://stackoverflow.com/questions/4911105/in-ruby-how-to-be-warned-of-duplicate-keys-in-hashes-when-loading-a-yaml-docume/55705853#55705853
def duplicate_keys(file_str)
  # src https://stackoverflow.com/questions/3328495/how-to-read-whole-file-in-ruby/3328510#3328510
  yaml = File.read(file_str)
  duplicate_keys = []

  validator = ->(node, parent_path) do
    if node.is_a?(Psych::Nodes::Mapping)
      children = node.children.each_slice(2) # In a Mapping, every other child is the key node, the other is the value node.
      duplicates = children.map { |key_node, _value_node| key_node }.group_by(&:value).select { |_value, nodes| nodes.size > 1 }

      duplicates.each do |key, nodes|
        # skip yaml imports
        next if key == '<<'
        duplicate_key = {
          key: parent_path + [key],
          occurrences: nodes.map { |occurrence| "line: #{occurrence.start_line + 1}" },
        }.compact

        # because raise looks odd here
        puts "ERROR in file #{file_str} key #{duplicate_key[:key][-1]} duplicate in #{duplicate_key[:occurrences]}"
        exit 1

        duplicate_keys << duplicate_key
      end

      children.each { |key_node, value_node|
         validator.call(value_node, parent_path + [key_node.value].compact)
      }
    else
      node.children.to_a.each { |child| validator.call(child, parent_path) }
    end
  end

  ast = Psych.parse_stream(yaml)
  validator.call(ast, [])

  duplicate_keys
end

def read_vars(myPath)
  # file that merges all yaml files
  allfile = myPath + 'all.yml'
  if File.exists? allfile
    File.delete(allfile)
  end

  # helper: copy example files
  if ! File.exists? myPath + '10-globals.yml' and ! File.exists? myPath + '30-nodes.yml'
    puts("Looks like this is newly git cloned repository, copying example yaml files to use them:")
    system("cp -v #{myPath + '10-globals.yml.example'} #{myPath + '10-globals.yml'}")
    system("cp -v #{myPath + '30-nodes.yml.example'} #{myPath + '30-nodes.yml'}")
  end

  all_f = File.open(allfile, 'a')
  # look for all yml files
  Dir[myPath + '*.yml'].sort.each {|file|
    duplicate_keys(file)
    next if file == './all.yml'
    # read file -> src https://stackoverflow.com/a/131001
    temp = File.open(file, 'r').read
    all_f.write(temp)
  }
  all_f.close

  nodes = Psych.load_file(allfile)
  return nodes
end

# generate all nodes defined in yaml configuration
def generate_all(myPath)
  nodes = read_vars(myPath)

  # init firmware paths
  fw_paths = []
  # src https://stackoverflow.com/a/32230037
  nodes['network'].each {|k, v|
    if v.nil?
      puts "WARNING: Node #{k} is empty"
      # https://stackoverflow.com/questions/4230322/in-ruby-how-do-i-skip-a-loop-in-a-each-loop-similar-to-continue/4230350#4230350
      next
    elsif v['node_name'].nil?
      # convert key of the entire subarray as value node_name
      v['node_name'] = k
    end
    # we want all nodes to include these variables (they can be overridden)
    v_n = prepare_global_variables(v, myPath)
    # TODO this way, when we are in no file provisioning we are not getting
    # nice temba vars inside fw
    if v['file_provision'] == 'nofilebase'
      zipfile = generate_firmware(v_n, myPath)
    else
      zipfile = generate_node(v_n, myPath)
    end
    fw_paths.push(zipfile)
  }
  if fw_paths.length > 1
    puts("------------------------------------------------")
    puts("\nSUMMARY of all temba nodes generated\n\n")
    fw_paths.map { |p| puts "  #{__dir__}/output/#{File.basename(p)}" }
    puts("\n")
  end
end

def prepare_global_variables(node_cfg, myPath)
  # timestamp to uniquely identify this build
  #   web form already did the timestamp
  node_cfg['timestamp'] = gen_timestamp() if node_cfg['timestamp'].nil?
  # check file provision mode -> src https://stackoverflow.com/questions/1986386/check-if-a-value-exists-in-an-array-in-ruby/1986398#1986398
  ['template','static','nofilebase'].include? node_cfg.fetch('file_provision')
  # packages are generated through a merge of yaml arrays -> src https://stackoverflow.com/questions/24090177/how-to-merge-yaml-arrays
  # it is required to postprocess with flatten function in ruby, and to put the array as a string separated by whitespaces
  # packages can be repeated by different sets of 15-packages.yml
  node_cfg['packages'] = node_cfg.fetch('packages').flatten.uniq.join(' ')
  platform = node_cfg.fetch('platform')
  platform_type = node_cfg.fetch('platform_type')
  # guess what imagebuilder to use
  # inspired by https://stackoverflow.com/questions/247948/is-there-a-better-way-of-checking-nil-or-length-0-of-a-string-in-ruby/251644#251644
  if node_cfg['image_base'].to_s.length > 0
    node_cfg['image_base'] = myPath + node_cfg.fetch('image_base')
  elsif node_cfg['image_base_local_suffix'].to_s.length > 0
    ib_losu = node_cfg['image_base_local_suffix']
    if ! ib_losu.nil?
      ib_losu = "__#{node_cfg['image_base_local_suffix']}"
    else
      ib_losu = ''
    end
    openwrt = node_cfg['openwrt']
    node_cfg['image_base'] = myPath + "imagebuilder_local/openwrt-imagebuilder-#{platform}-#{platform_type}.Linux-x86_64#{ib_losu}"
  else  # build official image builder
    openwrt_version = node_cfg.fetch('openwrt_version')
    openwrt_number = openwrt_version.split('.')[0]
    node_cfg['openwrt_number'] = openwrt_number
    if openwrt_version == 'snapshot'
      node_cfg['download_base'] = "https://downloads.openwrt.org/snapshots/targets/#{platform}/#{platform_type}/"
      node_cfg['image_base'] = myPath + "openwrt-imagebuilder-#{platform}-#{platform_type}.Linux-x86_64"
    else
      node_cfg['download_base'] = "https://downloads.openwrt.org/releases/#{openwrt_version}/targets/#{platform}/#{platform_type}/"
      node_cfg['image_base'] = myPath + "openwrt-imagebuilder-#{openwrt_version}-#{platform}-#{platform_type}.Linux-x86_64"
    end

    # DEBUG
    # the path between different architectures is different (for example in cases of ar71xx and x86)
    #if node_cfg['platform'] == 'x86' or node_cfg['profile'] == 'tl-wdr4300-v1'
    #    node_cfg['image_base'] = myPath + "openwrt-imagebuilder-#{openwrt_version}-#{platform}-#{platform_type}.Linux-x86_64"
    #else
    #    node_cfg['image_base'] = myPath + "openwrt-imagebuilder-#{platform}-#{platform_type}.Linux-x86_64"
    #end
    prepare_official_ib(node_cfg)
  end

  # check that the imagebuilder to use is found
  raise "\nERROR: #{File.expand_path(node_cfg['image_base'])} not found\n\n" if ! Dir.exists? node_cfg['image_base']
  return node_cfg
end

def generate_node(node_cfg, myPath)

  node_name = node_cfg['node_name']

  if $debug_erb
    dir_name = myPath + "debug-" + node_name
  else
    dir_name = "#{node_cfg['image_base']}/files"
  end

  raise "\n\nERROR: filebase variable is empty\n\n" if node_cfg['filebase'].nil?
  prepare_directory(dir_name, myPath + node_cfg['filebase'], node_cfg)

  # SSID is guifi.net/node_name, truncated to the ssid limit (32 characters)
  wifi_ssid_base = node_cfg['wifi_ssid_base']
  # don't use unless -> src https://veerasundaravel.wordpress.com/2011/02/25/why-ruby-unless-doesnt-have-elsif-or-elsunless-option/
  if ! wifi_ssid_base.nil?
    node_cfg['wifi_ssid'] = (wifi_ssid_base + node_name).slice(0,32)
  elsif ! node_cfg['wifi_ssid'].nil?
    puts 'WARNING: "wifi_ssid_base" variable is undefined. Custom "wifi_ssid" is going to be used'
  end

  # avoid redundant data entry in yaml (ip4_cidr in CIDR vs ip4 and netmask4)
  ip4_cidr = node_cfg['ip4_cidr']
  unless ip4_cidr.nil?
    temp_ip, temp_netmask = ip4_cidr.split("/")

    # TODO Need newer version of gem -> src https://github.com/ipaddress-gem/ipaddress/blob/master/lib/ipaddress.rb#L157-L161
    #unless IPAddress.valid_ipv4_subnet? ip4_cidr
    unless IPAddress.valid_ipv4?(temp_ip) && (!(temp_netmask =~ /\A([12]?\d|3[0-2])\z/).nil? || IPAddress.valid_ipv4_netmask?(temp_netmask))
      raise 'ipv_cidr: invalid IP address'
    end

    ip4 = IPAddress::IPv4.new ip4_cidr
    node_cfg['ip4'] = ip4.address
    node_cfg['netmask4'] = ip4.netmask
  end

  # TODO this and above should be more encapsulated on functions
  g10sr = node_cfg['guifi10_static_route']
  unless g10sr.nil?
    # TODO Need newer version of gem -> src https://github.com/ipaddress-gem/ipaddress/blob/master/lib/ipaddress.rb#L157-L161
    unless IPAddress.valid_ipv4?(g10sr)
      raise 'guifi10_static_route: invalid IP address'
    end
  end

  #Evaluate templates
  locate_erb(dir_name, node_cfg)

  # end function here when you are just debugging templating
  if $debug_erb
    print('Directory debug-', node_cfg['node_name'], "...  Done!\n")
    return
  end

  return generate_firmware(node_cfg, myPath)

end

def prepare_directory(dir_name,filebase, node_cfg)

  # Clean up
  FileUtils.rm_r dir_name if File.exists? dir_name
  dir_name_template = File.dirname(dir_name) + '/files_template'
  FileUtils.rm_r dir_name_template if File.exists? dir_name_template

  # Prepare (copy recursively the directory preserving permissions and dereferencing symlinks)
  system('cp -rpL ' + filebase + ' ' + dir_name) or raise "\n\nERROR: Template failed to be copied. Maybe there is a broken symlink\n\n"

  # temba dynamic pseudorelease that appears as banner
  temba_version = ' Temba ' + get_current_temba_commit() + "\n"
  temba_hline = ' -----------------------------------------------------' + "\n"

  # create dinamically a file to identify temba firmware with specific branch and commit
  # src https://stackoverflow.com/questions/2777802/how-to-write-to-file-in-ruby#comment24941014_2777863

  # directory exists -> src https://stackoverflow.com/questions/1085218/how-to-check-if-a-given-directory-exists-in-ruby/1085260#1085260
  puts "\n\nWARNING! looks like there is no etc directory inside filebase path. That does not look good!\n\n" unless Dir.exists? node_cfg['filebase'] + '/etc/'
  # temba always put some files in /etc (independently of the template used) -> src https://stackoverflow.com/questions/19280341/create-directory-if-it-doesnt-exist-with-ruby/19280532#19280532
  FileUtils.mkdir_p dir_name + '/etc/'

  File.write(dir_name + '/etc/temba_commit', get_current_temba_commit())
  File.write(dir_name + '/etc/temba_banner', temba_version + temba_hline)

  timestamp = node_cfg.fetch('timestamp')

  # include variables in the yaml
  node_cfg['timestamp'] = timestamp
  node_cfg['temba_commit'] = get_current_temba_commit()
  node_cfg['temba_version'] = temba_version.strip
  # set a default password when it is indefined
  node_cfg['passwd'] = '13f' if node_cfg['passwd'].nil?
  # format password for /etc/shadow
  node_cfg['hashed_passwd'] = node_cfg['passwd'].crypt('$1$md5Salt$')

  File.write( dir_name + '/etc/temba_vars.yml', node_cfg.to_yaml)

  unless node_cfg['bmx6_drop_ipv6_links'].nil?
    # add template for bmx6 drop links
    # file heredoc -> https://blog.saeloun.com/2020/04/08/heredoc-in-ruby-and-rails.html#input-redirection
    # erb each -> structure -> https://teamtreehouse.com/community/loop-through-array-in-an-erb-template
    # TODO check valid ipv6
    File.open("#{dir_name}/etc/firewall.user.erb", 'w') do |f|
      f << <<HEREDOC
<% @node.fetch('bmx6_drop_ipv6_links').each do |n| %>
# <%= n.fetch('name') %>
ip6tables -A INPUT -p all -s <%= n.fetch('ip6') %> -j DROP
<%end%>
HEREDOC
    end
  end

  # duplicate directory in order to maintain a copy of erb variables
  FileUtils.cp_r dir_name, File.dirname(dir_name) + '/files_template'
end

def locate_erb(dir_name, node_cfg)
  Dir.glob("#{dir_name}/**/*.erb").each do |erb_file|
    basename = erb_file.gsub '.erb',''
    process_erb(node_cfg,erb_file,basename)
  end
end

def process_erb(node,erb,base)
  @node = node
  # enable trim mode -> src https://stackoverflow.com/questions/4632879/erb-template-removing-the-trailing-line
  safe_level = nil # TODO this erb operation might be insecure (specially for ror app?)  SecurityError: Insecure operation - eval
  template = ERB.new(File.new(erb).read, safe_level, '-')
  # catch error -> src https://medium.com/@farsi_mehdi/error-handling-in-ruby-part-i-557898185e2f
  begin
  # rails require binding context -> src https://blog.revathskumar.com/2014/10/ruby-rendering-erb-template.html
    File.open(base, 'w') { |file| file.write(template.result(binding)) }
  rescue KeyError => e
    puts "Template error in file #{File.basename(erb)} of #{node['filebase']}: contains undefined variables.\n  #{e.message}"
    abort
  end
  FileUtils.rm erb
end

def generate_firmware(node_cfg, myPath)

  # everything goes to tmp. when zip file is finished it goes to parent directory
  out_dir_base = myPath + 'output/tmp'
  # src https://stackoverflow.com/questions/5020710/copy-a-file-creating-directories-as-necessary-in-ruby
  FileUtils.mkdir_p out_dir_base

  node_name = node_cfg.fetch('node_name')
  profile = node_cfg.fetch('profile')
  profile_bin = node_cfg['profile_bin']
  # next is probably the situation for all target/linux/ar71xx/image/legacy.mk -> src https://bugs.openwrt.org/index.php?do=details&task_id=2061
  profile_bin = profile if node_cfg['profile_bin'].nil?
  packages = node_cfg.fetch('packages')

  image_base = node_cfg['image_base']

  # do small images in x86 architectures -> src https://stackoverflow.com/questions/7290871/ruby-how-to-replace-text-in-a-file/18913856#18913856
  if node_cfg['small_rootfs'].nil? || node_cfg['small_rootfs'] == false
  ib_config_path=node_cfg['image_base'] + '/.config'
  IO.write(ib_config_path, File.open(ib_config_path) do |f|
      # quits the default value (reduces from any number to ~50 MB) -> src https://forum.openwrt.org/t/how-to-set-root-filesystem-partition-size-on-x86-imabebuilder/4765
      f.read.gsub(/CONFIG_TARGET_ROOTFS_PARTSIZE=[0-9]*/, "CONFIG_TARGET_ROOTFS_PARTSIZE=32")
    end
  )
  end

  make_cmd="make -C #{image_base} image PROFILE=#{profile} PACKAGES='#{packages}'"
  if node_cfg['file_provision'] != 'nofilebase'
    make_cmd="#{make_cmd} FILES=./files"
  end

  puts("\n\n\n\n\n    >>> #{make_cmd}\n\n\n\n\n")
  # throw error on system call -> src https://stackoverflow.com/a/18728161
  system(make_cmd) or raise "Openwrt build error. Check dependencies and requirements. Check consistency of:\n    #{image_base}\n    or the archive where came from #{File.basename(image_base)}.tar.xz"

  # notes for the output file
  notes = node_cfg['notes']
  if(notes)
    notes = '__' + notes.gsub(' ', '-')
  else
    notes = ''
  end

  timestamp = node_cfg['timestamp']
  out_dir = out_dir_base + '/' + node_name + '_' + timestamp
  Dir.mkdir out_dir

  zipfile = "#{out_dir_base}/#{node_name}_#{timestamp}.zip"
  platform = node_cfg['platform']
  platform_type = node_cfg['platform_type']
  openwrt = node_cfg['openwrt']
  openwrt_version = node_cfg['openwrt_version']

  # TODO all this if block section should be highly refactored
  # different platforms different names in output file
  #if "#{platform}-#{platform_type}" == "x86-64"
  if "#{platform}" == "x86"
    out_path = "#{out_dir}/#{node_name}#{notes}-combined-ext4.img.gz"

    src_path_dir = "#{image_base}/bin/targets/#{platform}/#{platform_type}"
    src_path = Dir.glob("#{src_path_dir}/openwrt*-#{platform}*-combined-ext4.img.gz")[0]

    # thanks https://apidock.com/ruby/File/expand_path/class
    if src_path.nil?
      raise "\n\n  ~> Target file does not exist, read previous lines about what happened. Does firmware fit this device?\nMaybe you are compiling a rare device. Check build directory:\n    #{File.expand_path(src_path_dir)}\n\n"
    end

    # TODO this differs from "else" situation because there is not "-#{profile_bin}", changes the final text, and there is no sysupgrade image (just one image)
    FileUtils.mv(src_path, out_path)

    # this requires so much space and is slow
    #system("gunzip -f -k #{out_dir}/#{node_name}-combined-ext4.img.gz")

    Archive::Zip.archive(zipfile, out_path)
  else
    out_path = {'sysupgrade' => "#{out_dir}/#{node_name}#{notes}-sysupgrade.bin",
                'factory'    => "#{out_dir}/#{node_name}#{notes}-factory.bin"}

    src_path_dir = "#{image_base}/bin/targets/#{platform}/#{platform_type}"
    src_path = Dir.glob("#{src_path_dir}/openwrt*-#{platform}-#{platform_type}-#{profile_bin}-squashfs-sysupgrade.bin")[0]

    if src_path.nil?
      raise "\n\n  ~> Target file does not exist, read previous lines about what happened. Does firmware fit this device?\nMaybe you are compiling a rare device. Check build directory:\n    #{File.expand_path(src_path_dir)}\n\n"
    end

    # process sysupgrade image: move to a different directory and compact it
    # TODO in "path" situation (image builder from stratch) "-#{openwrt_version}" must not be there
    FileUtils.mv(src_path, out_path['sysupgrade'])

    # compact sysupgrade files in a zip
    Archive::Zip.archive(zipfile, out_path['sysupgrade'])

    # process factory image: move to a different directory and compact it
    # some devices does not have factory
    factory_path = Dir.glob("#{image_base}/bin/targets/#{platform}/#{platform_type}/openwrt*-#{platform}-#{platform_type}-#{profile_bin}-squashfs-factory.bin")[0]
    if ! factory_path.nil?
      FileUtils.mv(factory_path, out_path['factory'])
      Archive::Zip.archive(zipfile, out_path['factory'])
    end

  end

  if node_cfg['file_provision'] != 'nofilebase'
    # add README to explain the contents of the zipfile)
    File.write( out_dir + '/README.txt', 'Contained files:

-   *combined-ext4.img.gz: special image in case you built x86_64 architecture. You have to uncompress it (warning: 7 MB => 200 MB)
-   *sysupgrade.bin: use it if you are comming from openwrt firmware
-   *factory.bin: use it if you are comming from stock/OEM/factory firmware
-   variables.yml: the variables that defined the firmware you got
-   files: all files that are inside this firmware
-   files_template: all files without applying variables.yml (in case you want to know what is exactly templating)
')
    Archive::Zip.archive(zipfile, out_dir + '/README.txt')
    # add etc
    FileUtils.cp_r "#{image_base}/files/", out_dir + '/files'
    Archive::Zip.archive(zipfile, out_dir + '/files')
    # add files_template
    FileUtils.cp_r "#{image_base}/files_template/", out_dir + '/files_template'
    Archive::Zip.archive(zipfile, out_dir + '/files_template')
    # add variables.yml
    File.write( out_dir + '/variables.yml', node_cfg.to_yaml)
    Archive::Zip.archive(zipfile, out_dir + '/variables.yml')
  end
  # when the file is ready, put it in the place to be downloaded
  FileUtils.mv(zipfile, "#{out_dir_base}/..")

  puts("\ntemba firmware generated: #{__dir__}/output/#{File.basename(zipfile)}\n\n")

  return zipfile
end

# user should check consistency of image_base file and directory by itself
def prepare_official_ib(node_cfg)
  image_base = node_cfg.fetch('image_base')
  download_base = node_cfg.fetch('download_base')

  ib_archive = "#{image_base}.tar.xz"

  unless $debug_erb
    # rails case: ensure basename of ib_archive for concatenation, but place in the same place as temba cli
    system("wget -c #{download_base}#{File.basename(ib_archive)} -O #{ib_archive}") or raise "ERROR. Internet is unreachable or URL is incorrect. Variables: download_base=#{download_base}; ib_archive=#{ib_archive}\n \n"
    unless File.exists? image_base
      # tar to specific directory -> src https://www.tecmint.com/extract-tar-files-to-specific-or-different-directory-in-linux/
      system("tar xf #{ib_archive} --directory #{File.dirname(ib_archive)}") or raise "ERROR processing system call"
    end
  end
end
